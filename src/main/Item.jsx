import React from "react";

const Item = ({ item, handlePrdDetail}) => {
  return (
    <div key={item.id} className="col-4 mt-3">
      <div className="card">
        <img src={item.image} alt="..." />
        <div className="card-body">
          <p className="font-weight-bold"> {item.name}</p>
          <p className="mt-3">{item.price} </p>
          <button
            data-toggle="modal"
            data-target="#exampleModal"
            className="btn btn-outline-success"
            // item đang đại diện cho data.json
            onClick={() => handlePrdDetail(item)}
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
};

export default Item;
