import React from "react";
import Item from "./Item";

const ListProduct = ({ data,  handlePrdDetail }) => {
  return (
    <div className="row">
      {data.map((item) => {
        return <Item item={item} key={item.id}  handlePrdDetail={ handlePrdDetail} />;
      })}
    </div>
  );
};

export default ListProduct;
