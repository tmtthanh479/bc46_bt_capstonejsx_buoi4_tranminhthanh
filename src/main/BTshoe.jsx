import React, { useState } from "react";
import ListProduct from "./ListProduct";
import data from "./data.json";

const BTshoe = () => {
  // detail
  const [prdDetail, setprdDetail] = useState(data[[0]]);
  const handlePrdDetail = (productDetail) => {
    // gọi hàm của state phải truyền giá trị vd như "productDetail"
    setprdDetail(productDetail);
  };
  return (
    <div>
      <h1 className="text-center mt-5">Shoe Shop</h1>
      <ListProduct data={data} handlePrdDetail={handlePrdDetail} />
      <div>
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Chi tiết sản phẩm
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              {/* 2. tùy chỉnh body detail ở đây */}
              <div className="modal-body">
                <div className="row">
                  <div className="col-4">
                    {/* 2. dùng state dẫn tới data img */}
                    <img className="img-fluid" src={prdDetail.image} alt="..." />
                  </div>
                  <div className="col-8">
                    <p className="font-weight-bold">{prdDetail.name}</p>
                    <p className="mt-3">{prdDetail.description}</p>
                    <p className="font-weight-bold mt-3">{prdDetail.price}</p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BTshoe;
